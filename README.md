
# ImageUploadResizer

This is react component for form input type file. Component goal is to resize uploaded image and convert it to jpeg format encoded as base64 for later uses.

## Installation

This can also be installed with `npm`.

```sh
npm install react-form-upload-image-resize --save
```

Git clone

```sh
$ git clone git@bitbucket.org:dainiusgliebus/react-form-upload-image-resize.git
$ cd react-form-upload-image-resize
$ npm install
$ npm link

# in your project folder
$ npm link react-form-upload-image-resize
```

## Usage

Use `URLSearchParams` directly. You can instantiate a new instance of `URLSearchParams` from a string or an object.

```javascript
// import component into your react component
import ImageUploadResizer from 'react-form-upload-image-resize';

// put code where you want to have upload image input
<ImageUploadResizer change={callback} width={200} height={200} />
```

## Props

| Prop | Required | Value | Description
|--|--|--|--|
| height | true |int | Resized image max height (default 200px)
| width | true |int | Resized image max width (default 200px)
| change | true |callback | Callback function to get processed uploaded image
| id | false |string | Input element id
| name | false |string | Input element name
| style | false | obj | Style object
| classes | false | string | CSS classes

## LICENSE

MIT license