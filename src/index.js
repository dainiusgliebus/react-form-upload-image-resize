import React, { Component } from 'react';

class ImageUploadResizer extends Component {
    
    storeImage(e){
        const files = Array.from(e.target.files)

        if (!files[0].type.match('image.*')) {
            console.log('bad format file');
            return ;
        }
        
        const props = this.props;

        let fileReader = new FileReader();
        fileReader.readAsDataURL( files[0] );

        fileReader.onload = (e) => {
        
    		let tempImg = new Image();
            tempImg.src = e.target.result;
            tempImg.onload = function(e) {
            
                var MAX_WIDTH = props.width ? parseInt(props.width, 10) : 200;
                var MAX_HEIGHT = props.height ? parseInt(props.height, 10) : 200;
                var tempW = tempImg.width;
                var tempH = tempImg.height;
                if (tempW > tempH) {
                    if (tempW > MAX_WIDTH) {
                        tempH *= MAX_WIDTH / tempW;
                        tempW = MAX_WIDTH;
                    }
                } else {
                    if (tempH > MAX_HEIGHT) {
                        tempW *= MAX_HEIGHT / tempH;
                        tempH = MAX_HEIGHT;
                    }
                }
                const canvas = document.createElement('canvas');
                canvas.width = tempW;
                canvas.height = tempH;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(this, 0, 0, tempW, tempH);
                var dataURL = canvas.toDataURL("image/jpeg");

                props.change(dataURL);

            }
        }
        
	}

    render () {
        const name = this.props.name ? this.props.name : 'image';
        const id = this.props.id ? this.props.id : 'iur_image';
        const style = this.props.style ? this.props.style : null;
        const className = this.props.classes ? this.props.classes : null;
        return <input type="file" name={name} id={id} onChange={(e) => this.storeImage(e)} style={style} className={className}  />
    }
}

export default ImageUploadResizer;